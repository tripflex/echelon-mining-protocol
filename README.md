# Echelon Protocol
Version 1.0.3  
The echelon specification repository can be found [here](https://gitlab.com/crypto137/nexa/echelon-mining-protocol).

The stratum protocol is used by [ASIC](https://en.wikipedia.org/wiki/Application-specific_integrated_circuit) miners, devices created specifically for performing Bitcoin [mining](/protocol/blockchain/proof-of-work/mining).

The echelon protocol is a modified version of the stratum v1 protocol for mining nexa.

Miners use the echelon protocol to reach out to one or more servers (e.g. a mining pool) in order to obtain a mining candidate for a new [block](/protocol/blockchain/block).
The miner tests solutions by increasing the nonce until a candidate block that meets the specified difficulty requirements is successfully created.

## Message Format

Echelon uses the [JSON-RPC 2.0](https://en.wikipedia.org/wiki/JSON-RPC) message format.
That is, [JSON](https://en.wikipedia.org/wiki/JSON)-encoded messages, separated by newline (line feed) characters.
There are two high-level message formats: requests and responses.

### Requests

Requests all contain the following fields:

| Field | Format | Description |
|--|--|--|
| id | number or string | A message ID that must be unique per request that expects a response.  For requests not expecting a response (called notifications), this is null. |
| method | string | Indicates the type of request the message represents. |
| params | array | Additional data which varies based on the request method. |

### Responses

Responses all contain the following fields:

| Field | Format | Description |
|--|--|--|
| id | number or string | The ID of the request that this message is a response to. |
| result | any | Data being returned in response to the request.  Must be present, but may be a string, number, array, object, or null. |
| error | [error array](#error-array-format) | Indicates that the request could not be fulfilled and provides information about what went wrong. |

#### Error Array Format

The error array is a JSON array containing the following elements:

| Field | Format | Description |
|--|--|--|
| error code | number | An error code indicating the type of error. |
| message | string | A description of the error. |
| data | object | Additional data associated with the error (nullable). |

*Example error:* <code>{"result":null,"id":2,"error":[24,"Unauthorized worker",null]}</code>

Beyond those specified by JSON-RPC, the following error codes are used:

| Code | Description |
|--|--|
| 20 | Other/Unknown |
| 21 | Job not found (=stale) |
| 22 | Duplicate share |
| 23 | Low difficulty share |
| 24 | Unauthorized worker |
| 25 | Not subscribed |

## Echelon Protocol Flow

```
Client                                Server   
  |                                     |   
  | --------- mining.subscribe -------> |   
  | --------- mining.authorize -------> |   
  | <-------- mining.set_difficulty --- |   
  |                                     |----   
  | <---------- mining.notify --------- |<--/   
  |                                     |   
  | ---------- mining.submit ---------> |   
```

### Full Protocol

### Client Methods

#### mining.subscribe

Upon connecting to a server, clients are expected to send a subscribe request, described as follows.

| Field | Format | Description |
|--|--|--|
| method | string | "mining.subscribe" |
| params\[0\] - user_agent_version | string | User agent name and version, separated by a forward slash (e.g. "nxminer/2.0.0"). |

*Example request:* <code>{"id": 1, "method": "mining.subscribe", "params": ["nxminer/2.0.0"]}</code>

In response, the server will send the following data in its result object, which is a JSON array:

| Field | Format | Description |
|--|--|--|
| subscriptions | array | An array containing sub-array that described intended subscriptions.  The first value in sub-arrays is the subscription type (e.g. "mining.set_difficulty"), the second is a subscription ID. |
| extranonce | string | The 64 bit big endian hex encoded value that is to be used the first 8 bytes of the solution nonce. |
| extranonce2_size | int | The number of bytes that the miner users for its extranonce2 counter. |

*Example response:* <code>{"result":[[["mining.set_difficulty","731ec5e0649606ff"]],"0000000000000001", 8],"id":1,"error":null}</code>

### mining.authorize

In order to authenticate itself, the client send an authorize message:

| Field | Format | Description |
|--|--|--|
| method | string | "mining.authorize" |
| params\[0\] - username | string | The client's user name. |
| params\[1\] - password | string | The client's password (if required by the server). |

*Example request:* <code>{"id": 1, "method": "mining.authorize", "params": ["username", "p4ssw0rd"]}</code>

Response format:

| Field | Format | Description |
|--|--|--|
| result | boolean | The value true if the client has been authorized; false otherwise. |

*Example response:* <code>{"id": 2, "result": true, "error": null}</code>

### mining.submit

Once the client has completed a job (received via [mining.notify](#miningnotify) following an accepted [mining.authorize](#miningauthorize)), it returns its result using a submit message.
This message is also used to submit shares to a mining pool.
The format is as follows:

| Field | Format | Description |
|--|--|--|
| method | string |"mining.submit" |
| params\[0\] - username | string | The client's user name. |
| params\[1\] - job_id | string | The job ID for the work being submitted. |
| params\[2\] - candidate_id | string | The 64 bit big endian hex-encoded value of the candidate id. (same value as received in mining.notify) |
| params\[3\] - header_commitment | string | The 256 bit big endian hex-encoded value of the header commitment. (same value as received in mining.notify)|
| params\[4\] - nbits | string | The 32 bit big endian hex-encoded value of the block difficulty. (same value as received in mining.notify)|
| params\[5\] - solution_nonce | string | The 128 bit big endian hex-encoded value of the solution nonce. |
| params\[6\] - time | string | The 64 bit big endian hex-encoded value of the time. (same value as received in mining.notify)|

*Example request:* <code>{"id": 1, "method": "mining.submit", "params": ["username", "4f", "0000000000007a47", "e9c64cfd6711c5eaa8d20dc4c4a430b1e22141ecc1ad701471492420432d8a4b", "1b02fab2", "000000000000000100003e05486566fd", "0000000063b1fb60"]}</code>

Response format:

| Field | Format | Description |
|--|--|--|
| result | boolean | The value true if the submission has been accepted; false if it was rejected. |

*Example response:* <code>{"id": 2, "result": true, "error": null}</code>

### mining.suggest_difficulty(preferred share difficulty Number)
Used to indicate a preference for share difficulty to the pool. Servers are not required to honour this request, even if they support the stratum method.


## Server Methods

### mining.notify

Once a client successfully connects using the [mining.subscribe](#miningsubscribe) and [mining.authorize](#miningauthorize) messages, the server may send a job to the client, informing it of everything it needs to know in order to mine a new block.
The notify message is a notification (does not expect a response) and has the following format:

| Field | Format | Description |
|--|--|--|
| method | string | "mining.notify" |
| params\[0\] - job_id | string | The job ID for the job being sent in this message. |
| params\[1\] - candidate_id | string | The 64 bit big endian hex-encoded candidate id. |
| params\[2\] - header_commitment | string | The 256 bit big endian hex-encoded header commitment. |
| params\[3\] - nbits | string | The 32 bit big endian hex-encoded network difficulty required for the block. |
| params\[4\] - time | string | The 64 bit big endian hex-encoded current time for the block. |
| params\[5\] - clean | boolean string (true/false) | Indicates whether the client should forget any prior jobs.  If true, the server will reject any submissions for prior jobs and the miner should forget any prior job IDs so that they can be reused by the server. |
| params\[6\] - extra_nonce | string | The 64 bit big endian hex encoded value that is to be used the first 8 bytes of the solution nonce. May be different than the extra_nonce sent in the mining.subscribe response. |


*Example request:* <code>{"id": null, "method": "mining.notify", "params": ["4f", "0000000000007a47", "e9c64cfd6711c5eaa8d20dc4c4a430b1e22141ecc1ad701471492420432d8a4b", "1b02fab2", "0000000063b1fb60", false, "0000000000000001"]}</code>


## mining.set_difficulty

The server can adjust the difficulty required for miner shares with the "mining.set_difficulty" method. The miner should begin enforcing the new difficulty on the next job received. Some pools may force a new job out when set_difficulty is sent, using clean_jobs to force the miner to begin using the new difficulty immediately.

| Field | Format | Description |
|--|--|--|
| method | string | "mining.set_difficulty" |
| params\[0\] - new_difficulty | number | The new difficulty threshold for share reporting.  Shares are reported using the [mining.submit](#miningsubmit) message. |

*Example request:* <code>{ "id": null, "method": "mining.set_difficulty", "params": [2]}</code>
